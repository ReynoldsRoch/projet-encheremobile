import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent } from "@ionic/react";
import EnchereComponent from "../components/EnchereComponent";

const Enchere: React.FC = () => {
    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Ionic - Enchere</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <EnchereComponent></EnchereComponent>
            </IonContent>
        </IonPage>

    );
};

export default Enchere;