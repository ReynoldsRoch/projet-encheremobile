import { IonPage, IonHeader, IonToolbar, IonTitle, IonContent } from "@ionic/react";
import CompteComponent from "../components/CompteComponent";

const Compte: React.FC = () => {

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>Ionic - Enchere</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>

                <CompteComponent></CompteComponent>
            </IonContent>
        </IonPage>

    );
};

export default Compte;