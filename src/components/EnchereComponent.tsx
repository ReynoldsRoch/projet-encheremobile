import { IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonContent, IonFab, IonFabButton, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonList, IonModal, IonTitle, IonToolbar } from "@ionic/react";
import { OverlayEventDetail } from "@ionic/react/dist/types/components/react-component-lib/interfaces";
import { add, camera } from "ionicons/icons";
import { useEffect, useRef, useState } from "react";
import axios from 'axios';


const EnchereComponent: React.FC = () => {

    //photo enchere
    const [imgEnchere, setImgEnchere] = useState<any[]>([]);
    // const [imgEnchere, setImgEnchere] = useState(null);
    function getImgEnchere(ide: number) {
        console.log("ide : " + ide);
        var xhr = new XMLHttpRequest();

        var url = "http://localhost:8080/photoencheres/" + ide;

        xhr.open("GET", url, true);
        xhr.send();

        xhr.onreadystatechange = function () {
            if (xhr.status == 200 && xhr.readyState == 4) {
                var response = JSON.parse(xhr.responseText);
                setImgEnchere(response);
                console.log(imgEnchere);
            }
        }
    }

    // insert photoEnchere
    const [photo, setPhoto] = useState<any>(String);

    const openFileDialog = () => {
        (document as any).getElementById("file-upload").click();
    }
    const setImage = (_event: any) => {
        let f = _event.target.files![0];
        let reader = new FileReader();
        reader.readAsDataURL(f);
        reader.onload = () => {
            var result = reader.result;
            // console.log(result);
            setPhoto(result);
        }
    }

    // enchereCompteClient
    const [enchereCompteClient, setEnchereCompteClient] = useState<any[]>([]);
    function mesEncheres() {
        var xhr = new XMLHttpRequest();

        var idClient = sessionStorage["idClient"];

        var url = "http://localhost:8080/enchere?idClient=" + idClient;

        xhr.open("GET", url, true);
        xhr.send();

        xhr.onreadystatechange = function () {
            if (xhr.status == 200 && xhr.readyState == 4) {
                var response = JSON.parse(xhr.responseText);
                // console.log(response);
                setEnchereCompteClient(response);
            }
        }
    }

    useEffect(() => {

        mesEncheres();
        enchereCompteClient.map((enchere, index) => (
            getImgEnchere(enchere.id)
        ));

    })

    // const [images, setImages] = useState<{ [key: number]: string }>({});
    // useEffect(() => {
    //     async function fetchData() {
    //         mesEnchere();
    //         const encheres = await enchereCompteClient();
    //         encheres.forEach(async enchere => {
    //             const img = await getImgEnchere(enchere.id);
    //             setImages(prevImages => ({ ...prevImages, [enchere.id]: img }));
    //         });
    //     }
    //     fetchData();
    // }, []);

    // modal 
    const modal = useRef<HTMLIonModalElement>(null);
    const input = useRef<HTMLIonInputElement>(null);
    const [message, setMessage] = useState(
        'This modal example uses triggers to automatically open a modal when the button is clicked.'
    );

    // ajout enchere
    function confirm() {
        var description = (document.getElementById("description") as HTMLIonInputElement).value;
        var prix = (document.getElementById("prix") as HTMLIonInputElement).value;
        var duree = (document.getElementById("duree") as HTMLIonInputElement).value;
        var currentDate = new Date();
        var dateString = currentDate.toISOString().substring(0, 10);

        var idClient = sessionStorage["idClient"];

        var url = "http://localhost:8080/encheres?description=" + description + "&prix=" + prix + "&duree=" + duree + "&date=" + dateString + "&idClient=" + idClient;

        var xhr = new XMLHttpRequest();

        xhr.open("POST", url, true);
        xhr.send();

        console.log("url : " + url);

        var idEnchere = 0;

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                idEnchere = parseInt(xhr.responseText);
                // console.log(xhr.responseText);
                console.log("idEnchere = " + idEnchere);

                if (idEnchere != 0) {
                    const data = {
                        "idEnchere": idEnchere,
                        "name": photo
                    }
                    console.log(data);
                    console.log("\n");
                    console.log("\n");
                    console.log(photo);

                    url = "http://localhost:8080/photoencheres";
                    axios.post(url, data)
                        .then(response => {
                            console.log(response);
                        })
                        .catch(error => {
                            console.log(error);
                        }
                        );
                }

            }
        }

        modal.current?.dismiss(input.current?.value, 'confirm');
    }

    function onWillDismiss(ev: CustomEvent<OverlayEventDetail>) {
        if (ev.detail.role === 'confirm') {
            setMessage(`Hello, ${ev.detail.data}!`);
        }
    }

    return (
        <>
            <IonContent fullscreen={true} className="ion-padding">
                <h1>Mes encheres</h1>

                {sessionStorage["idClient"] != null ?
                    <>
                        <IonLabel>Hello client numero {sessionStorage["idClient"]}</IonLabel>

                        {enchereCompteClient != null ?
                            <>
                                <IonList>
                                    {enchereCompteClient.map((enchere) => (
                                        <IonItem key={enchere.id}>
                                            <IonCard>
                                                <>
                                                    {imgEnchere.map((value: string, id: number) => {
                                                        return (
                                                            <IonItem key={id}>
                                                                { imgEnchere && imgEnchere[id]["idEnchere"] == enchere.id ?
                                                                    <>
                                                                        <img width={"50%"} height={"50%"} src={imgEnchere[id]["name"]} />
                                                                    </>
                                                                    :
                                                                    <>
                                                                        <img alt="Silhouette of mountains" src="https://ionicframework.com/docs/img/demos/card-media.png" />
                                                                    </>
                                                                }
                                                            </IonItem>
                                                        );
                                                    })}


                                                    {/* {imgEnchere && imgEnchere.idEnchere == enchere.id ?
                                                        <>
                                                            <img width={"50%"} height={"50%"} src={imgEnchere.name} />
                                                        </>
                                                        :
                                                        <>
                                                            <img alt="Silhouette of mountains" src="https://ionicframework.com/docs/img/demos/card-media.png" />
                                                        </>
                                                    } */}

                                                </>
                                                <IonCardHeader>
                                                    <IonCardTitle>Enchere numero - {enchere.id}</IonCardTitle>
                                                    <IonCardSubtitle>PrixMin : {enchere.prixmin}</IonCardSubtitle>
                                                    <IonCardSubtitle>Status : {enchere.statue}</IonCardSubtitle>
                                                    <IonCardSubtitle>Date : {enchere.dateDebut}</IonCardSubtitle>

                                                </IonCardHeader>

                                                <IonCardContent>
                                                    <IonLabel>Description:</IonLabel>
                                                    {enchere.description}
                                                    <IonLabel>Acheteur:</IonLabel>
                                                    {enchere.acheteur}
                                                    <IonLabel>Mise:</IonLabel>
                                                    {enchere.montant}
                                                    <IonLabel>Le:</IonLabel>
                                                    {enchere.dateMise}
                                                </IonCardContent>

                                            </IonCard>
                                        </IonItem>
                                    ))}
                                </IonList>
                                <IonFab slot="fixed" vertical="bottom" horizontal="end" id="open-modal">
                                    <IonFabButton>
                                        <IonIcon icon={add}></IonIcon>
                                    </IonFabButton>
                                </IonFab>
                            </>
                            :
                            <>
                            </>}


                    </>
                    : <>
                        <IonLabel>Sorry, you need to <a href='/home'>sign up</a></IonLabel>
                    </>
                }

            </IonContent>

            {/* Modal */}
            <IonModal ref={modal} trigger="open-modal" onWillDismiss={(ev) => onWillDismiss(ev)}>
                <IonHeader>
                    <IonToolbar>
                        <IonButtons slot="start">
                            <IonButton onClick={() => modal.current?.dismiss()}>Cancel</IonButton>
                        </IonButtons>
                        <IonTitle>Ajouter Enchere</IonTitle>
                        <IonButtons slot="end">
                            <IonButton strong={true} onClick={() => confirm()}>
                                Confirm
                            </IonButton>
                        </IonButtons>
                    </IonToolbar>
                </IonHeader>

                <IonContent className="ion-padding">
                    <IonItem>
                        <IonLabel position="stacked">Description de l'enchere</IonLabel>
                        <IonInput ref={input} type="text" placeholder="Decrivez votre enchere" id="description" required value={"Description kely"} />
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Prix mise en enchere</IonLabel>
                        <IonInput ref={input} type="number" placeholder="Prix minimal de vente" id="prix" required min={0} value={20000} />
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Duree de votre enchere</IonLabel>
                        <IonInput ref={input} type="time" placeholder="Duree" id="duree" value={"01:45"} required />
                    </IonItem>
                    <IonItem>
                        <IonLabel position="stacked">Photo de votre enchere</IonLabel>
                        <input type={"file"} id="file-upload" onChange={setImage}></input>
                        <IonButton onClick={openFileDialog}><IonIcon slot="icon-only" icon={camera}></IonIcon></IonButton>
                        <img width={"50%"} height={"50%"} src={photo}></img>
                    </IonItem>
                </IonContent>
            </IonModal>

        </>
    );

}

export default EnchereComponent;