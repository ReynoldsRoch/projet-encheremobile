import React, { useEffect, useState } from 'react';
import { IonButton, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonContent, IonItem, IonLabel, useIonAlert } from '@ionic/react';

import './main.css';

interface ContainerProps {
}


const CompteComponent: React.FC<ContainerProps> = ({ }) => {

    // compte
    const [compte, setCompte] = useState<any[]>([]);
    function monCompte() {
        var idClient = sessionStorage["idClient"];

        var xhr = new XMLHttpRequest();
        var url = "http://localhost:8080/clients/" + idClient;

        xhr.open("GET", url, true);
        xhr.send();

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var response = JSON.parse(xhr.responseText);
                // console.log(response);
                setCompte(response);
            }
        }
    }

    useEffect(() => {
        monCompte();
    }, [])

    function disconnect() {
        sessionStorage.clear();
        window.location.href = "/home";
    }

    function confirm() {
        var idClient = sessionStorage["idClient"];
        var montant = (document.getElementById("montant") as HTMLIonInputElement).value;

        console.log(idClient);
        console.log(montant);

        var xhr = new XMLHttpRequest();
        var url = "http://localhost:8080/recharge?idClient=" + idClient + "&montant=" + montant;

        xhr.open("POST", url, true);
        xhr.send();

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                var response = xhr.responseText;
                if (response == "1") {
                    alert("Demande envoye");
                }
            }
        }

    }

    // loginAlert
    const [presentAlert] = useIonAlert();
    const [handlerMessage, setHandlerMessage] = useState('');
    const [roleMessage, setRoleMessage] = useState('');
    function loginAlert() {
        presentAlert({
            header: 'Demande Rechargement',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        setHandlerMessage('Alert canceled');
                    },
                },
                {
                    text: 'Valider',
                    role: 'confirm',
                    handler: () => {
                        setHandlerMessage('Alert confirmed');
                        confirm()
                    },
                },
            ],
            onDidDismiss: (e: CustomEvent) => setRoleMessage(`Dismissed with role: ${e.detail.role}`),
            inputs: [
                {
                    placeholder: 'Montant',
                    type: 'number',
                    id: 'montant',
                    value: '10000'
                }
            ],
        })
    }


    return (
        <>
            {sessionStorage["idClient"] == null ?
                <>
                    <IonContent fullscreen={true} className="ion-padding">
                        <h1>Mon Compte</h1>
                        <IonLabel>Sorry, you need to <a href='/home'>sign up</a></IonLabel>
                    </IonContent>
                </> :
                <>

                    {compte.map((value: string, id: number) => {
                        return (
                            <IonContent fullscreen={true} className="ion-padding">
                                <IonItem key={id}>
                                    <IonCard>
                                        <img alt="Silhouette of mountains" src="https://ionicframework.com/docs/img/demos/card-media.png" />
                                        <IonCardHeader>
                                            <IonCardTitle>
                                                Client numero -  {compte[id]["id"]}
                                            </IonCardTitle>
                                        </IonCardHeader>

                                        <IonCardContent>
                                            <IonLabel>nom : {compte[id]["nom"]}</IonLabel>
                                            <IonLabel>prenom : {compte[id]["prenom"]}</IonLabel>
                                            <IonLabel>Email : {compte[id]["email"]}</IonLabel>
                                            <IonLabel>contact : {compte[id]["contact"]}</IonLabel>
                                            <IonLabel>solde : {compte[id]["solde"]}</IonLabel>
                                        </IonCardContent>
                                        <IonButton fill="clear" onClick={loginAlert}>Recharger compte</IonButton>
                                        <IonButton fill="clear" color={"danger"} onClick={disconnect}>Disconnect</IonButton>
                                    </IonCard>

                                </IonItem>
                            </IonContent>
                        );

                    })}



                </>
            }
        </>
    );
}
export default CompteComponent;
