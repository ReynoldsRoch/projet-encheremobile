import { IonButton, IonButtons, IonContent, IonHeader, IonInput, IonItem, IonLabel, IonList, IonModal, IonTitle, IonToolbar, useIonAlert, useIonToast } from '@ionic/react';
import { OverlayEventDetail } from '@ionic/react/dist/types/components/react-component-lib/interfaces';
import { useRef, useState } from 'react';
import './ExploreContainer.css';

interface ContainerProps { }

const ExploreContainer: React.FC<ContainerProps> = () => {

  // inscription
  function inscription() {
    var Nom = (document.getElementById("idNom") as HTMLIonInputElement).value;
    var Prenom = (document.getElementById("idPrenom") as HTMLIonInputElement).value;
    var Contact = (document.getElementById("idContact") as HTMLIonInputElement).value;
    var Email = (document.getElementById("idEmail") as HTMLIonInputElement).value;
    var Mdp = (document.getElementById("idMdp") as HTMLIonInputElement).value;
    var Solde = (document.getElementById("idSolde") as HTMLIonInputElement).value;

    var xhr = new XMLHttpRequest();
    var url = "http://localhost:8080/clients?nom=" + Nom + "&prenom=" + Prenom + "&contact=" + Contact + "&email=" + Email + "&mdp=" + Mdp + "&solde=" + Solde;

    xhr.open("POST", url, true);
    xhr.send();

    console.log(url);

    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var result = JSON.parse(xhr.responseText);
        console.log(result);
        if (result=="1") {
          alert("INSCRIPTION REUSSI");
        }
      }
    }
  }

  // login
  function login() {

    var email = (document.getElementById("emailID") as HTMLIonInputElement).value;
    var password = (document.getElementById("passwordID") as HTMLIonInputElement).value;

    console.log(email);
    console.log(password);

    var xhr = new XMLHttpRequest();
    var url = "http://localhost:8080/login?email=" + email + "&mdp=" + password;

    xhr.open("GET", url, true);
    xhr.send();

    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var result = JSON.parse(xhr.responseText);
        console.log(result);
        if (result["message"] == "Correct") {
          sessionStorage.setItem("token", result["token"]);
          sessionStorage.setItem("idClient", result["id"]);
          
          window.location.reload()
          // message etsy ambony
          presentToast('top')

        }
      }
    }

  }

  // after login
  const [present] = useIonToast();

  const [message, setMessage] = useState(
    'This modal example uses triggers to automatically open a modal when the button is clicked.'
  );

  const presentToast = (position: 'top' | 'middle' | 'bottom') => {
    present({
      message: 'Login successfully!',
      duration: 2000,
      position: position
    });
  };

  // loginAlert
  const [presentAlert] = useIonAlert();
  const [handlerMessage, setHandlerMessage] = useState('');
  const [roleMessage, setRoleMessage] = useState('');
  function loginAlert() {
    presentAlert({
      header: 'Please enter your info',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            setHandlerMessage('Alert canceled');
          },
        },
        {
          text: 'Login',
          role: 'confirm',
          handler: () => {
            setHandlerMessage('Alert confirmed');
            login()
          },
        },
      ],
      onDidDismiss: (e: CustomEvent) => setRoleMessage(`Dismissed with role: ${e.detail.role}`),
      inputs: [
        {
          placeholder: 'Email',
          type: 'email',
          id: 'emailID',
          value: 'Rakoto@gmail.com'
        },
        {
          placeholder: 'Password',
          type: 'password',
          id: 'passwordID',
          value: 'rakoto123'
        },
      ],
    })
  }


  return (
    <>
      <IonLabel>
        <h1>INSCRIPTION & LOGIN</h1>
      </IonLabel>

      <IonItem>
        <IonLabel position="floating">Nom</IonLabel>
        <IonInput placeholder="Entrer votre nom" required id='idNom' value={"NOM"}></IonInput>
      </IonItem>

      <IonItem>
        <IonLabel position="floating">Prenom</IonLabel>
        <IonInput placeholder="Entrer votre prenom" required id='idPrenom' value={"Prenom"}></IonInput>
      </IonItem>

      <IonItem>
        <IonLabel position="floating">Email</IonLabel>
        <IonInput placeholder="Entrer votre email" required id='idEmail' value={"email@gmail.com"}></IonInput>
      </IonItem>

      <IonItem>
        <IonLabel position="floating">Contact</IonLabel>
        <IonInput placeholder="Entrer votre contact" required id='idContact' value={"0341234567"}></IonInput>
      </IonItem>

      <IonItem>
        <IonLabel position="floating">Mot de passe</IonLabel>
        <IonInput type='password' placeholder="Entrer votre mot de passe" required id='idMdp' value={"12345"}></IonInput>
      </IonItem>

      <IonItem>
        <IonLabel position="floating">Solde</IonLabel>
        <IonInput type='number' placeholder="Entrer votre solde" required id='idSolde' value={10000}></IonInput>
      </IonItem><br />

      <IonButton onClick={inscription}>S'inscrire</IonButton><br />
      <IonButton onClick={loginAlert}>Log in</IonButton>

    </>
  );
};

export default ExploreContainer;
